package com.ivrom.revenuestatistics.service;

import com.ivrom.revenuestatistics.model.entity.Revenue;
import com.ivrom.revenuestatistics.model.entity.Statistics;
import com.ivrom.revenuestatistics.repository.RevenueRepository;
import com.ivrom.revenuestatistics.repository.StatisticsRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StatisticsServiceTest {

    @InjectMocks
    private StatisticsService statisticsService;

    @Mock
    private StatisticsRepository statisticsRepository;
    @Mock
    private RevenueRepository revenueRepository;

    @Test
    void findLast() {
        statisticsService.findLast();

        verify(statisticsRepository, times(1))
                .findTopByOrderByGeneratedWhenDesc();
    }

    @Test
    void generateStatistics() {
//        Revenue revenue1 = Revenue.builder()
//                .id("1")
//                .value(100.0)
//                .date(LocalDateTime.now())
//                .build();
//        Revenue revenue2 = Revenue.builder()
//                .id("2")
//                .value(200.0)
//                .date(LocalDateTime.now())
//                .build();
//        List<Revenue> revenueList = List.of(revenue1, revenue2);
//        when(revenueRepository.findAll())
//                .thenReturn(revenueList);
//        when(statisticsRepository.save(any(Statistics.class)))
//                .thenAnswer(i -> i.getArgument(0));
//
//        statisticsService.generateStatistics();
//
//        ArgumentCaptor<Statistics> argument = ArgumentCaptor.forClass(Statistics.class);
//        verify(statisticsRepository).save(argument.capture());
//        Statistics savedStatistics = argument.getValue();
//
//        assertEquals(savedStatistics.getAverageValue(), 150.0);
//        assertEquals(savedStatistics.getMinValue(), 100.0);
//        assertEquals(savedStatistics.getMaxValue(), 200.0);
//        assertEquals(savedStatistics.getValuesNumber(), 2);
//        assertEquals(savedStatistics.getSum(), 300.0);
//        assertNotNull(savedStatistics.getGeneratedWhen());
    }
}