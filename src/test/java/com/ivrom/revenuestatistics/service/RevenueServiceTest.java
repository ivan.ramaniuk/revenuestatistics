package com.ivrom.revenuestatistics.service;

import com.ivrom.revenuestatistics.model.entity.Revenue;
import com.ivrom.revenuestatistics.repository.RevenueRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RevenueServiceTest {

    @InjectMocks
    private RevenueService revenueService;

    @Mock
    private RevenueRepository revenueRepository;

    @BeforeEach
    void setMockOutput() {
        when(revenueRepository.save(any(Revenue.class)))
                .thenAnswer(i -> i.getArgument(0));
    }

    @Test
    void whenCreateRevenue_thenReturnResult() {
        LocalDateTime now = LocalDateTime.now();
        Double value = 150.0;
        Revenue revenueToCreate = Revenue.builder()
                .date(now)
                .value(value)
                .build();

        Revenue createdRevenue = revenueService.createRevenue(revenueToCreate);

        assertEquals(createdRevenue.getDate(), now);
        assertEquals(createdRevenue.getValue(), value);
    }

    @Test
    void whenCreateRevenueWithEmptyDate_thenReturnResultWithDate() {
        Double value = 150.0;
        Revenue revenueToCreate = Revenue.builder()
                .value(value)
                .build();

        Revenue createdRevenue = revenueService.createRevenue(revenueToCreate);

        assertNotNull(createdRevenue.getDate());
    }

}