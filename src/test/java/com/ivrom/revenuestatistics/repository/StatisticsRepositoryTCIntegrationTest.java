package com.ivrom.revenuestatistics.repository;

import com.ivrom.revenuestatistics.model.entity.Statistics;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Testcontainers
@SpringBootTest
class StatisticsRepositoryTCIntegrationTest {

    @Container
    protected final static PostgreSQLContainer<?> postgreSQLContainer =
            new PostgreSQLContainer<>("postgres:13.1")
                    .withDatabaseName("integration-tests-db")
                    .withUsername("postgres")
                    .withPassword("postgres")
                    .withExposedPorts(5432);

    @DynamicPropertySource
    private static void postgresqlProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
    }

    static {
        postgreSQLContainer.start();
    }

    @Autowired
    protected StatisticsRepository statisticsRepository;


    @Test
    public void shouldFindLastStatistics() {
        LocalDateTime initialDateTime = LocalDateTime.now();
        Statistics stat1 = Statistics.builder()
                .averageValue(5.0)
                .generatedWhen(initialDateTime.minusSeconds(30))
                .build();
        Statistics stat2 = Statistics.builder()
                .generatedWhen(initialDateTime.minusSeconds(20))
                .build();
        Statistics stat3 = Statistics.builder()
                .generatedWhen(initialDateTime.minusSeconds(10))
                .build();
        Statistics stat4 = Statistics.builder()
                .generatedWhen(initialDateTime)
                .build();

        statisticsRepository.save(stat1);
        statisticsRepository.save(stat2);
        statisticsRepository.save(stat3);
        Statistics firstFoundStat = statisticsRepository.findTopByOrderByGeneratedWhenDesc();

        statisticsRepository.save(stat4);
        Statistics secondFoundStat = statisticsRepository.findTopByOrderByGeneratedWhenDesc();

        assertEquals(firstFoundStat.getId(), stat3.getId());
        assertEquals(secondFoundStat.getId(), stat4.getId());
    }

}