package com.ivrom.revenuestatistics.service;

import com.ivrom.revenuestatistics.model.entity.Revenue;
import com.ivrom.revenuestatistics.repository.RevenueRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class RevenueService {

    private final RevenueRepository revenueRepository;

    public RevenueService(RevenueRepository revenueRepository) {
        this.revenueRepository = revenueRepository;
    }

    public Revenue createRevenue(Revenue revenue) {

        if (revenue.getDate() == null) {
            revenue.setDate(LocalDateTime.now());
        }
        return revenueRepository.save(revenue);
    }
}
