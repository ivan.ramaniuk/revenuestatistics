package com.ivrom.revenuestatistics.service;

import com.ivrom.revenuestatistics.model.entity.Revenue;
import com.ivrom.revenuestatistics.model.entity.Statistics;
import com.ivrom.revenuestatistics.repository.RevenueRepository;
import com.ivrom.revenuestatistics.repository.StatisticsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StatisticsService {

    private final StatisticsRepository statisticsRepository;
    private final RevenueRepository revenueRepository;

    public StatisticsService(StatisticsRepository statisticsRepository,
                             RevenueRepository revenueRepository) {
        this.statisticsRepository = statisticsRepository;
        this.revenueRepository = revenueRepository;
    }

    public Statistics findLast() {
        return statisticsRepository.findTopByOrderByGeneratedWhenDesc();
    }

    public void generateStatistics() {
        List<Revenue> revenuesList = revenueRepository.findAll();

        if (CollectionUtils.isEmpty(revenuesList)) {
            log.info("There is no Revenues to generate statistics");
            return;
        }

        Statistics statistics = Statistics.builder()
                .generatedWhen(LocalDateTime.now())
                .averageValue(revenuesList.stream().collect(Collectors.averagingDouble(Revenue::getValue)))
                .minValue(Collections.min(revenuesList, Comparator.comparing(Revenue::getValue)).getValue())
                .maxValue(Collections.max(revenuesList, Comparator.comparing(Revenue::getValue)).getValue())
                .sum(revenuesList.stream().mapToDouble(Revenue::getValue).sum())
                .valuesNumber(revenuesList.size())
                .build();
        statisticsRepository.save(statistics);
    }

}
