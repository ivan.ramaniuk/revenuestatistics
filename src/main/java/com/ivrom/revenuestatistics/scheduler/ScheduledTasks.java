package com.ivrom.revenuestatistics.scheduler;

import com.ivrom.revenuestatistics.service.StatisticsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;

@Slf4j
@Component
public class ScheduledTasks {

    private final ExecutorService executorService;
    private final StatisticsService statisticsService;

    public ScheduledTasks(ExecutorService executorService,
                          StatisticsService statisticsService) {
        this.executorService = executorService;
        this.statisticsService = statisticsService;
    }

    @Scheduled(cron = "0 */5 * * * *")
    public void generateStatisticsTask() {
        log.info("Run Generate statistics by cron");
        executorService.execute(statisticsService::generateStatistics);
    }
}
