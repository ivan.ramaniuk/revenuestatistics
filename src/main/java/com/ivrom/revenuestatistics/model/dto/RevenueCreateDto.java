package com.ivrom.revenuestatistics.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class RevenueCreateDto {

    private Double value;
    private LocalDateTime date;
}
