package com.ivrom.revenuestatistics.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class StatisticsResponse {

    private String id;
    private LocalDateTime generatedWhen;
    private Double averageValue;
    private Double minValue;
    private Double maxValue;
    private int valuesNumber;
    private Double sum;
}
