package com.ivrom.revenuestatistics.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class RevenueResponse {

    private String id;
    private Double value;
    private LocalDateTime date;
}
