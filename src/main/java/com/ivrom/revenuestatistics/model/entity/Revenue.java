package com.ivrom.revenuestatistics.model.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Revenue {

    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();

    private Double value;
    private LocalDateTime date;

}
