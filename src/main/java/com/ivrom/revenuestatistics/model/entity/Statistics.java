package com.ivrom.revenuestatistics.model.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Statistics {

    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();

    @Column(nullable = false)
    private LocalDateTime generatedWhen;
    private Double averageValue;
    private Double minValue;
    private Double maxValue;
    private int valuesNumber;
    private Double sum;

}
