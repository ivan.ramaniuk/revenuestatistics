package com.ivrom.revenuestatistics.model.mapper;

import com.ivrom.revenuestatistics.model.dto.StatisticsResponse;
import com.ivrom.revenuestatistics.model.entity.Statistics;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StatisticsMapper {

    StatisticsResponse toResponse(Statistics statistics);
    List<StatisticsResponse> toResponseList(List<Statistics> revenue);
}
