package com.ivrom.revenuestatistics.model.mapper;

import com.ivrom.revenuestatistics.model.dto.RevenueCreateDto;
import com.ivrom.revenuestatistics.model.dto.RevenueResponse;
import com.ivrom.revenuestatistics.model.entity.Revenue;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RevenueMapper {

    Revenue toRevenue(RevenueCreateDto revenueDto);

    RevenueResponse toResponse(Revenue revenue);

    List<RevenueResponse> toResponseList(List<Revenue> revenue);
}
