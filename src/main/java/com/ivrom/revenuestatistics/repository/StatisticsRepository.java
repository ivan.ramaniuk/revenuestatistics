package com.ivrom.revenuestatistics.repository;

import com.ivrom.revenuestatistics.model.entity.Statistics;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatisticsRepository extends JpaRepository<Statistics, String> {

    Statistics findTopByOrderByGeneratedWhenDesc();
}