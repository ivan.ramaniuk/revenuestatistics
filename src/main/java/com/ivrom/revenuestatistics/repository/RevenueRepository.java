package com.ivrom.revenuestatistics.repository;

import com.ivrom.revenuestatistics.model.entity.Revenue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RevenueRepository extends JpaRepository<Revenue, String> {
}
