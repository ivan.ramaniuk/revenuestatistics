package com.ivrom.revenuestatistics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RevenueStatisticsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RevenueStatisticsApplication.class, args);
	}

}
