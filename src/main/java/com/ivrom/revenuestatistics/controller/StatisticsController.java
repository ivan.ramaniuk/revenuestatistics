package com.ivrom.revenuestatistics.controller;

import com.ivrom.revenuestatistics.model.dto.StatisticsResponse;
import com.ivrom.revenuestatistics.model.entity.Statistics;
import com.ivrom.revenuestatistics.model.mapper.StatisticsMapper;
import com.ivrom.revenuestatistics.service.StatisticsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/statistics")
@Slf4j
public class StatisticsController {

    private final StatisticsService statisticsService;
    private final StatisticsMapper statisticsMapper;

    public StatisticsController(StatisticsService statisticsService,
                                StatisticsMapper statisticsMapper) {
        this.statisticsService = statisticsService;
        this.statisticsMapper = statisticsMapper;
    }

    @GetMapping
    @ResponseBody
    public StatisticsResponse findLast() {
        log.info("Request to get the last Statistics");
        Statistics foundStatistics = statisticsService.findLast();

        return statisticsMapper.toResponse(foundStatistics);
    }

}
