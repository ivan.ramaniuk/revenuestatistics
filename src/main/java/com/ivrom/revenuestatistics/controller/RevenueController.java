package com.ivrom.revenuestatistics.controller;

import com.ivrom.revenuestatistics.model.dto.RevenueCreateDto;
import com.ivrom.revenuestatistics.model.dto.RevenueResponse;
import com.ivrom.revenuestatistics.model.entity.Revenue;
import com.ivrom.revenuestatistics.model.mapper.RevenueMapper;
import com.ivrom.revenuestatistics.service.RevenueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/revenues")
@Slf4j
public class RevenueController {

    private final RevenueService revenueService;
    private final RevenueMapper revenueMapper;

    public RevenueController(RevenueService revenueService, RevenueMapper revenueMapper) {
        this.revenueService = revenueService;
        this.revenueMapper = revenueMapper;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public RevenueResponse addRevenue(@RequestBody RevenueCreateDto revenueCreateDto) {
        log.info("Request to create new Revenue: {}", revenueCreateDto);

        Revenue revenueToCreate = revenueMapper.toRevenue(revenueCreateDto);
        Revenue createdRevenue = revenueService.createRevenue(revenueToCreate);

        return revenueMapper.toResponse(createdRevenue);
    }

}
