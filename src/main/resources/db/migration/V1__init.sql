create table revenue
(
    id varchar(36) not null
        constraint revenue_pkey
            primary key,
    date  timestamp,
    value double precision
);

alter table revenue
    owner to postgres;

create table statistics
(
    id varchar(36) not null
        constraint statistics_pkey
            primary key,
    average_value  double precision,
    generated_when timestamp    not null,
    max_value      double precision,
    min_value      double precision,
    sum            double precision,
    values_number  integer      not null
);

alter table statistics
    owner to postgres;